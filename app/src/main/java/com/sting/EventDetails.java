package com.sting;



import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kvsatya on 6/14/15.
 */
public class EventDetails implements Serializable {
    String event_info;

    public String getEvent_refer_url() {
        return event_refer_url;
    }

    public void setEvent_refer_url(String event_refer_url) {
        this.event_refer_url = event_refer_url;
    }

    public String getEvent_info() {
        return event_info;
    }

    public void setEvent_info(String event_info) {
        this.event_info = event_info;
    }

    public String getHosted_at() {
        return hosted_at;
    }

    public void setHosted_at(String hosted_at) {
        this.hosted_at = hosted_at;
    }

    public String getLoc_info() {
        return loc_info;
    }

    public void setLoc_info(String loc_info) {
        this.loc_info = loc_info;
    }

    public ArrayList<Agenda> getAgenda() {
        return agenda;
    }

    public void setAgenda(ArrayList<Agenda> agenda) {
        this.agenda = agenda;
    }

    public ArrayList<Hostinfo> getHosters() {
        return hosters;
    }

    public void setHosters(ArrayList<Hostinfo> hosters) {
        this.hosters = hosters;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    String event_refer_url;
    String hosted_at;
    String loc_info;
    ArrayList<Agenda> agenda;
    ArrayList<Hostinfo> hosters;
    ArrayList<Tag> tags;
    ArrayList<Attendee> attendees;




}
