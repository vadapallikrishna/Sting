package com.sting;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;

/**
 * Created by kvsatya on 6/13/15.
 */
public class SearchActivity extends AppCompatActivity {

   private RecyclerView recyclerView;


   Double Latitude,Longitude;
   private Context context;
    ArrayList<Event> Events;

   String type;
   @Override
   protected void onCreate(Bundle icicle){
       super.onCreate(icicle);
       type=null;
       setContentView(R.layout.activity_type);
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       recyclerView = (RecyclerView)findViewById(R.id.recycle);
       Latitude = getIntent().getDoubleExtra("Lat",0);
       Longitude = getIntent().getDoubleExtra("Lon",0);
       type = getIntent().getStringExtra("type");
       getSupportActionBar().setTitle(type);
       recyclerView.setLayoutManager(new LinearLayoutManager(this));
       context = this;




   }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("STINGX",0);
        if(type== null)
           type = sharedPreferences.getString("TYPE","");
        getSupportActionBar().setTitle(type);
        if(Events == null) {
            createEvents();
        }else {
            recyclerView.setAdapter(new EventsAdapter(Events, context));
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences sharedPreferences = getSharedPreferences("STINGX",0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("TYPE",type);
        editor.commit();



    }

    public void createEvents(){
        Apiservice s = new Apiservice();
        s.getEventsService().getEventsByType(type, new retrofit.Callback<List<Event>>() {
            @Override
            public void success(List<Event> events, retrofit.client.Response response) {
                Events = (ArrayList) events;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView.setAdapter(new EventsAdapter(Events, context));
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }







}
