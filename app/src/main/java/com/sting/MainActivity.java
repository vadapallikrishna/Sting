package com.sting;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;



import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceDetectionApi;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;



public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,LocationListener,ResultCallback<People.LoadPeopleResult> {

    private GoogleApiClient mGoogleApiClient;
    public String location;
    private static Location mlocation;
    private PlaceDetectionApi pd;
    private boolean providerEnabled;
    private static TextView loc;
    private static AutoCompleteTextView searchview;
    private static ArrayList<Address> AddList;
    private TextView user;
    private static DrawerLayout drawerLayout;

    private static boolean loggedIn;
    private boolean intentProgress;
    private boolean allowlogin;
    private int RC_SIGN_IN = 4;
    private ImageView cover;
    private static boolean useLocationApi;
    private static User u;
    private static Toolbar toolbar;
    private static Context c;
    static String[] TYPES;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        c =this;
        useLocationApi = false;
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                                    .addApi(Places.GEO_DATA_API)
                                    .addApi(Places.PLACE_DETECTION_API)
                                    .addApi(LocationServices.API)
                                    .addApi(Plus.API,Plus.PlusOptions.builder().build())
                                    .addScope(Plus.SCOPE_PLUS_LOGIN)
                                    .addScope(Plus.SCOPE_PLUS_PROFILE)
                                    .addConnectionCallbacks(this)
                                    .addOnConnectionFailedListener(this)
                                    .build();


        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        ab.setDisplayHomeAsUpEnabled(true);

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        drawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.PrimaryDark));
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.opdr,R.string.cldr){
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        NavigationView view = (NavigationView)findViewById(R.id.nav_view);
        view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                drawerLayout.closeDrawers();

                if(menuItem.isChecked()) return false;
                else menuItem.setChecked(true);

               if(loggedIn) {
                   switch (menuItem.getItemId()) {
                       case R.id.my_events:
                           if(u==null){
                               showTextBar("Wait");
                           }else{

                                getSupportFragmentManager().beginTransaction().replace(R.id.mev, new MyEventsFragment()).commit();
                           }
                           return true;
                       case R.id.nav_home:
                           getSupportFragmentManager().beginTransaction().replace(R.id.mev, new SearchFragment()).commit();
                           return true;
                       case R.id.mark:
                           getSupportFragmentManager().beginTransaction().replace(R.id.mev, new BookmarksFragment()).commit();
                           return true;
                   }
               }else{
                 showLoginBar();
               }
                return false;
            }
        });


         user = (TextView)findViewById(R.id.username);

       RelativeLayout header = (RelativeLayout)findViewById(R.id.header);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              if(!mGoogleApiClient.isConnecting()) {
                  if (loggedIn) {
                      if (mGoogleApiClient.isConnected()) {
                          Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                          mGoogleApiClient.disconnect();
                          user.setText("Login");
                          cover.setImageDrawable(null);
                          freeUser(u);
                          loggedIn=false;
                      }
                  } else {
                      allowlogin=true;
                      mGoogleApiClient.connect();
                  }
              }
                drawerLayout.closeDrawers();
            }
        });

        cover=(ImageView)findViewById(R.id.cover);


        getSupportFragmentManager().beginTransaction().replace(R.id.mev,new SearchFragment()).commit();




    }

    protected void onActivityResult(final int requestCode, final int resultCode,
                                    final Intent data) {
        if (requestCode == 2 && resultCode == RESULT_OK) {
            String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            user.setText(accountName);
            SharedPreferences pref = getSharedPreferences("STINGX",0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("User",accountName);
            editor.commit();
            loggedIn= true;
        }



        if (requestCode == RC_SIGN_IN) {
            intentProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }

        if(requestCode == 10){
           String t = data.getStringExtra("N");
           showCreatedBar(t);
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {
        if(useLocationApi) {
            mlocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mlocation != null)
                setLocation(mlocation);
            if (providerEnabled)
                createLocationRequest();
        }
        Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(this);
        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            Person curPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            loggedIn=true;
            user.setText(curPerson.getDisplayName());

            User u = new User();

            u.setUsername(Plus.AccountApi.getAccountName(mGoogleApiClient).substring(0,Plus.AccountApi.getAccountName(mGoogleApiClient).indexOf("@")));
            u.setEmail(Plus.AccountApi.getAccountName(mGoogleApiClient));
            Apiservice s = new Apiservice();
            s.getUser(this,u);
            Glide.with(c).load(curPerson.getCover().getCoverPhoto().getUrl()).into(cover);
        }else {
            Log.v("h","uns");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
      mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

       if(!intentProgress && connectionResult.hasResolution()){
          if(allowlogin) {
              try {
                  intentProgress = true;
                  startIntentSenderForResult(connectionResult.getResolution().getIntentSender(), RC_SIGN_IN, null, 0, 0, 0);
              } catch (IntentSender.SendIntentException e) {
                  intentProgress = false;
                  mGoogleApiClient.connect();
              }
          }
           allowlogin=false;
       }
    }

    @Override
    protected void onPause() {
        super.onPause();
       // LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,this);
    }





    protected void createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }


    @Override
    protected void onStop() {
        super.onStart();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mlocation = location;
        if(mlocation != null)
            setLocation(mlocation);

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        providerEnabled = true;
    }

    @Override
    public void onProviderDisabled(String provider) {
        providerEnabled = false;
    }

    public void setLocation(Location mlocation)  {
        Geocoder geocoder = new Geocoder(this);

        try {
            AddList = (ArrayList<Address>)geocoder.getFromLocation(mlocation.getLatitude(), mlocation.getLongitude(), 2);
            loc.setText(AddList.get(0).getLocality());
        }catch (IOException e){
            e.printStackTrace();
        }

     }

    public void checkText(){
        if(Arrays.asList(TYPES).contains(searchview.getText().toString())){
            Intent intent = new Intent(this,SearchActivity.class);
            intent.putExtra("Lat",mlocation.getLatitude());
            intent.putExtra("Lon",mlocation.getLongitude());
            intent.putExtra("type",searchview.getText().toString());
            startActivity(intent);
        }else{
            searchview.setText("");
            searchview.setHint("Events Not Available");
        }
    }

    @Override
    public void onResult(People.LoadPeopleResult loadPeopleResult) {
        if (loadPeopleResult.getStatus().getStatusCode() == CommonStatusCodes.SUCCESS) {

        } else {
            Log.e("h", "Error" + loadPeopleResult.getStatus());
        }
        loadPeopleResult.release();
    }

    private static void showLoginBar(){
        Snackbar snackbar =   Snackbar.make(drawerLayout,"Login",Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(c.getResources().getColor(R.color.rtt));
        snackbar.show();
    }

    private  void showCreatedBar(String t){
        Snackbar snackbar =   Snackbar.make(drawerLayout,t+" Event Created",Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(c.getResources().getColor(R.color.rtt));
        snackbar.show();

    }

    private  void showTextBar(String t){
        Snackbar snackbar =   Snackbar.make(drawerLayout,t,Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(c.getResources().getColor(R.color.rtt));
        snackbar.show();

    }

    public void setUpUser(User u){
        this.u=u;
    }

    public void freeUser(User u){
        this.u=null;
    }

    public static User getUser(){
        return u;
    }

    public static class MyEventsFragment extends Fragment{
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View v = inflater.inflate(R.layout.myevents_fragment, container, false);
            toolbar.setTitle("MyEvents");
            return v;
        }
    }

    public static class SearchFragment extends Fragment{
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View v = inflater.inflate(R.layout.search_main, container, false);
            toolbar.setTitle("Sting");
            loc = (TextView)v.findViewById(R.id.location);
            RelativeLayout rel = (RelativeLayout)v.findViewById(R.id.rels);

            searchview = (AutoCompleteTextView)v.findViewById(R.id.searchtext);
            TYPES = new String[]{"Music","Comedy","Magic","Meetup","Workshop"};
            ArrayAdapter<String> serviceTypes = new ArrayAdapter<String>(c,R.layout.service_item,TYPES);
            searchview.setAdapter(serviceTypes);

            ImageView sbtn = (ImageView)v.findViewById(R.id.sbutton);
            sbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivity a= (MainActivity)c;
                    a.checkText();
                }
            });
            searchview.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        MainActivity a= (MainActivity)c;
                        a.checkText();
                        return true;
                    }
                    return false;
                }
            });

            if(!useLocationApi) {
                LocationManager locationManager = (LocationManager) c.getApplicationContext().getSystemService(LOCATION_SERVICE);

                mlocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                SearchFragment.setLocation(mlocation);
            }


            FloatingActionButton fab = (FloatingActionButton)v.findViewById(R.id.newevent);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(loggedIn) {
                        Intent intent = new Intent(c, NewEventActivity.class);
                        startActivityForResult(intent, 10);
                    }else{
                        showLoginBar();
                    }
                }
            });
            return v;
        }

        public static void setLocation(Location mlocation)  {
            Geocoder geocoder = new Geocoder(c);

            try {
                AddList = (ArrayList<Address>)geocoder.getFromLocation(mlocation.getLatitude(), mlocation.getLongitude(), 2);
                loc.setText(AddList.get(0).getLocality());
            }catch (IOException e){
                e.printStackTrace();
            }

        }
    }

    public static class BookmarksFragment extends Fragment{
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View v = inflater.inflate(R.layout.bookmarks_fragment, container, false);
            toolbar.setTitle("Bookmarks");
            return v;
        }
    }

    @Override
    public void onBackPressed() {

    }
}
