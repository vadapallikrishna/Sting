package com.sting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by kvsatya on 6/13/15.
 */
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.VH> {

    ArrayList<Event> Events;
    Context c;
    Activity a;
    public EventsAdapter(ArrayList<Event> Events, Context c){
        this.Events = Events;
        this.c  = c ;
        a = (Activity)c;

    }


    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_item, parent, false);

        VH vh = new VH(v);

        return vh;


    }

    @Override
    public int getItemCount() {
        return Events.size();
    }

    @Override
    public void onBindViewHolder(final VH holder, int position) {
           final Event e = Events.get(position);
           holder.title.setText(e.getTitle().replace(e.getTitle().substring(0,0),e.getTitle().substring(0,0).toUpperCase()));
           Glide.with(c).load(e.getIco()).into(holder.eventimg);
           holder.time.setText(e.getEvent_details().getAgenda().get(0).getTime()+" - "+e.getEvent_details().getAgenda().get(1).getTime());
           holder.view.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Intent intent = new Intent(c,EventDetailActivity.class);

                   Bundle bundle = new Bundle();
                   bundle.putSerializable("Event",e);
                   intent.putExtras(bundle);
                   c.startActivity(intent);
               }
           });
           holder.attend.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {


                   Snackbar snackbar =   Snackbar.make(a.getWindow().getDecorView().findViewById(android.R.id.content), " Event was added to Your Events", Snackbar.LENGTH_LONG);
                   View snackBarView = snackbar.getView();
                   snackBarView.setBackgroundColor(c.getResources().getColor(R.color.rtt));
                   snackbar.show();
                   Apiservice a = new Apiservice();
                   Attendee t = new Attendee();
                   if(MainActivity.getUser() !=null ){
                       t.setUid(MainActivity.getUser().getId());
                       a.userAttend(t);
                   }
               }
           });

           holder.mark.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Snackbar snackbar =   Snackbar.make(a.getWindow().getDecorView().findViewById(android.R.id.content)," Event marked",Snackbar.LENGTH_LONG);
                   View snackBarView = snackbar.getView();
                   snackBarView.setBackgroundColor(c.getResources().getColor(R.color.rtt));
                   snackbar.show();
                   Apiservice a = new Apiservice();
                   Bookmark b = new Bookmark();
                   b.setEid(e.getId());
                   a.userMark(b);
               }
           });
    }

    public class VH extends RecyclerView.ViewHolder{
        CardView view;
        TextView title;
        ImageView eventimg;
        TextView hosted;
        TextView time;
        Button mark;
        Button attend;
        public VH(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.name);
            eventimg = (ImageView) itemView.findViewById(R.id.eventimg);
            time = (TextView)itemView.findViewById(R.id.time);
            hosted = (TextView)itemView.findViewById(R.id.hosting);
            view = (CardView)itemView.findViewById(R.id.eventcard);
            attend =(Button)itemView.findViewById(R.id.attend);
            mark = (Button)itemView.findViewById(R.id.mark);
          //  Oph = (TextView) itemView.findViewById(R.id.oph);
        }
    }
}
