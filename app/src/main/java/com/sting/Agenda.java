package com.sting;

import java.io.Serializable;

/**
 * Created by kvsatya on 6/14/15.
 */
public class Agenda implements Serializable {
    String time;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    String info;
}
