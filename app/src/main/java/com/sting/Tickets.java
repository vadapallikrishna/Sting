package com.sting;

import java.io.Serializable;

/**
 * Created by kvsatya on 6/14/15.
 */
public class Tickets implements Serializable {
    public String getTicket_type() {
        return ticket_type;
    }

    public void setTicket_type(String ticket_type) {
        this.ticket_type = ticket_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    String ticket_type;
    String price;
}
