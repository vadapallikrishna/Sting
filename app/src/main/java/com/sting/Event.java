package com.sting;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kvsatya on 6/13/15.
 */
public class Event implements Serializable {

    String id;
    String title;
    boolean open;
    String type;
    String ico;
    String Ontime;
    String created_by;
    String header;
    EventDetails event_details;
    ArrayList<Tickets> tickets;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public String getOntime() {
        return Ontime;
    }

    public void setOntime(String ontime) {
        Ontime = ontime;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public EventDetails getEvent_details() {
        return event_details;
    }

    public void setEvent_details(EventDetails event_details) {
        this.event_details = event_details;
    }

    public ArrayList<Tickets> getTickets() {
        return tickets;
    }

    public void setTickets(ArrayList<Tickets> tickets) {
        this.tickets = tickets;
    }












}
