package com.sting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

import okio.BufferedSink;

import static com.sting.R.layout.*;


/**
 * Created by kvsatya on 6/14/15.
 */
public class NewEventActivity extends AppCompatActivity {

    private static Event e;
    private static Button button;
    private static Context c;
    private static Activity a;
    @Override
    public void onCreate(Bundle icicle){
        super.onCreate(icicle);
        setContentView(activity_new_event);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        button = (Button)findViewById(R.id.steps);
        c = this;
        a = this;
        e = new Event();
        Step1Fragment step1 = new Step1Fragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container,step1);
        ft.addToBackStack(null);
        ft.commit();

    }

    public static class Step1Fragment extends Fragment{
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View v = inflater.inflate(new_event, container, false);
            final Spinner spinner = (Spinner)v.findViewById(R.id.type);
            String[] TYPES = new String[]{"Music","Comedy","Magic","Meetup","Workshop"};
            ArrayAdapter<String> serviceTypes = new ArrayAdapter<String>(v.getContext(), R.layout.spinner_item,TYPES);
            final EditText title = (EditText)v.findViewById(R.id.title);
            final EditText desc = (EditText)v.findViewById(R.id.desc);
            CheckBox op = (CheckBox)v.findViewById(R.id.op);

            button.setText("SUBMIT");
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(title.getText().toString().length() !=0 || desc.getText().toString().length() != 0) {

                        EventDetails ed = new EventDetails();
                        ed.setEvent_info(desc.getText().toString());
                        e.setEvent_details(ed);

                        e.setTitle(title.getText().toString());
                        e.setType(spinner.getSelectedItem().toString());

                        createRequest(e,v);
                    }
                }
            });
            spinner.setAdapter(serviceTypes);
            return v;
        }
    }

    public static class Step2Fragment extends Fragment{
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            return inflater.inflate(new_event_host, container, false);
        }
    }

    public static void createRequest(final Event e, final View v){
        if(e!=null){
            OkHttpClient okHttpClient = new OkHttpClient();
            String json = new Gson().toJson(e, Event.class);
            RequestBody requestBody = RequestBody.create(MediaType.parse(json),json);

            Request request = new Request.Builder().url("http://sleepy-atoll-4621.herokuapp.com/events").post(requestBody).build();
            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    a.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Intent intent = new Intent();
                            intent.putExtra("N",e.getTitle());
                            a.setResult(10,intent);
                            a.finish();
                        }
                    });


                }
            });

        }
    }


}
