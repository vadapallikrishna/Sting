package com.sting;

import android.app.Activity;
import android.util.Log;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by kvsatya on 6/19/15.
 */
public  class Apiservice {

    RestAdapter restAdapter;

    public Apiservice(){
        restAdapter = new RestAdapter.Builder().setEndpoint("http://peaceful-bastion-2145.herokuapp.com/api").build();
    }

    public interface EventsService{
        @GET("/events")
        void getEventsByType(@Query("filter[where][type]") String type,Callback<List<Event>> cb);

        @GET("/events")
        void getEventsByTags(@Query("filter[where][tag]") String tag,Callback<List<Event>> cb);

        @GET("/events")
        void getEventsByuser(@Query("filter[where][tag]") String user,Callback<List<Event>> cb);

        @PUT("/events")
        void attendEvent(@Body Attendee user,Callback<Event> cb);
    }

    public interface UsersService{
        @GET("/users/findone")
        void checkUser(@Query("filter[where][username]") String username,Callback<User> cb);

        @FormUrlEncoded
        @POST("/users")
        void createUser(@Field("username") String name,@Field("email") String email,Callback<User> cb);

        @PUT("/events")
        void markEvent(@Body Bookmark mark,Callback<User> cb);
    }

    public EventsService getEventsService(){
        return restAdapter.create(EventsService.class);
    }

    public void getUser(final MainActivity a, final User u){
             final UsersService s = restAdapter.create(UsersService.class);
             s.checkUser(u.getUsername(),new Callback<User>() {
                 @Override
                 public void success(User user, Response response) {
                     a.setUpUser(user);
                 }

                 @Override
                 public void failure(RetrofitError error) {
                     if(error.getResponse().getStatus()==404){
                          s.createUser(u.getUsername(),u.getEmail(),new Callback<User>() {
                              @Override
                              public void success(User user, Response response) {
                                  a.setUpUser(user);
                              }

                              @Override
                              public void failure(RetrofitError error) {
                                     error.printStackTrace();
                              }
                          });
                     }
                 }
             });


    }

    public void userAttend(Attendee a){
        EventsService e = restAdapter.create(EventsService.class);
        e.attendEvent(a,new Callback<Event>() {
            @Override
            public void success(Event event, Response response) {
                Log.v("h","success");
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                Log.v("h",""+error.getMessage());
            }
        });
    }

    public void userMark(Bookmark b){
        UsersService e = restAdapter.create(UsersService.class);
        e.markEvent(b,new Callback<User>(){

            @Override
            public void success(User user, Response response) {
                Log.v("h","success");
            }

            @Override
            public void failure(RetrofitError error) {
                  error.printStackTrace();
            }
        });
    }


}
