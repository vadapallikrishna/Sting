package com.sting;

import java.io.Serializable;

/**
 * Created by kvsatya on 6/19/15.
 */
public class Hostinfo implements Serializable{

    String name;
    String info;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }


}
