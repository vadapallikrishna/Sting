package com.sting;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kvsatya on 6/14/15.
 */
public class EventDetailActivity extends AppCompatActivity {
    int EVENTID;
    private TabLayout tabLayout;
    private ViewPager pager;
    private static Event e;
    @Override
    public  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
         e = (Event)getIntent().getExtras().getSerializable("Event");
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(e.getTitle());

        tabLayout = (TabLayout)findViewById(R.id.tabs);
        pager = (ViewPager)findViewById(R.id.viewpager);


        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new DetailFragment(),"DETAILS");
        if(e.getType().equalsIgnoreCase("Music")){

            adapter.addFragment(new SpeakerFragment(),"ARTISTS");

        }else{
            adapter.addFragment(new SpeakerFragment(),"SPEAKERS");
        }

        adapter.addFragment(new AgendaFragment(),"AGENDA");
        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager);
       // collapsingToolbar.setTitle(e.getTitle());
      //  final ImageView backdrop = (ImageView) findViewById(R.id.backdrop);

      //  Glide.with(this).load(e.getHeader()).centerCrop().into(backdrop);
     //   TextView t = (TextView)findViewById(R.id.title);
      //  t.setText(e.getTitle());


//        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Snackbar snackbar =   Snackbar.make(v," Event was added to Your Events",Snackbar.LENGTH_LONG);
//                View snackBarView = snackbar.getView();
//                snackBarView.setBackgroundColor(getResources().getColor(R.color.accent));
//                snackbar.show();
//            }
//        });
        final Context c = this;
        Button btn = (Button)findViewById(R.id.atte);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar =   Snackbar.make(v," Event was added to Your Events",Snackbar.LENGTH_LONG);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(c.getResources().getColor(R.color.rtt));
                snackbar.show();
            }
        });
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }


    public static class DetailFragment extends Fragment{
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View v = inflater.inflate(R.layout.fragment_detail, container, false);
            TextView about = (TextView)v.findViewById(R.id.about);
            about.setText(e.getEvent_details().getEvent_info());

            return v;
        }
    }

    public static class SpeakerFragment extends Fragment{
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View v = inflater.inflate(R.layout.fragment_speakers, container, false);
            TextView hostabout = (TextView)v.findViewById(R.id.host);
            TextView hostname = (TextView)v.findViewById(R.id.hosters);
            TextView hostinfo = (TextView)v.findViewById(R.id.hostinfo);


            hostname.setText(e.getEvent_details().getHosters().get(0).getName());
            hostinfo.setText(e.getEvent_details().getHosters().get(0).getInfo());
            return v;
        }
    }

    public static class AgendaFragment extends Fragment{
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View v = inflater.inflate(R.layout.fragment_agenda, container, false);
            TextView agenda = (TextView)v.findViewById(R.id.agenda);
            agenda.setText(e.getEvent_details().getAgenda().get(0).getInfo()+" "+e.getEvent_details().getAgenda().get(0).getTime());
            agenda = (TextView)v.findViewById(R.id.agenda1);
            agenda.setText(e.getEvent_details().getAgenda().get(1).getInfo()+" "+e.getEvent_details().getAgenda().get(1).getTime());
            return v;
        }
    }


}
